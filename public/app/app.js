'use strict';

var app = angular.module('app', []);

app.controller('MainCtrl',
	function($scope, $rootScope) {

	$scope.input = {};
	$scope.send = function() {
		try {
			if(!WebSocket) {
				alert('No websocket support');
			} else {
				var socket = WebSocket('ws://127.0.0.1:7778/');

				socket.addEventListener('open', function(e) {
					console.log('Open: ', e);
				});

				socket.addEventListener('error', function(e) {
					console.log('Error: ', e);
				})

				socket.addEventListener('message', function(e) {
					console.log('Message: ', JSON.parse(e.data));
				})

				console.log('Socket: ', socket);

				window.socket = socket;
			}
		} catch(e) {
			console.log('Exception: ' + e);
		}
	}

});