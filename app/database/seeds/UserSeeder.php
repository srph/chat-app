<?php

use Faker\Factory as Faker;

class UserSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$db = DB::table('users');
		$db->truncate();

		$faker = Faker::create();

		$data = array(
			array(
				'id'		=>	1,
				'username'	=>	$faker->userName,
				'password'	=>	Hash::make('pass')
			),

			array(
				'id'		=>	2,
				'username'	=>	$faker->userName,
				'password'	=>	Hash::make('pass')
			),

			array(
				'id'		=>	3,
				'username'	=>	$faker->userName,
				'password'	=>	Hash::make('pass')
			)
		);

		$db->insert($data);
	}
}