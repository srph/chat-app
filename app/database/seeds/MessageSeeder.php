<?php

use Faker\Factory as Faker;

class MessageSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$db = DB::table('messages');
		$db->truncate();

		$faker = Faker::create();

		$data = array(
			array(
				'id'		=>	1,
				'user_id'	=>	1,
				'content'	=>	$faker->paragraph(1),
				'created_at'=>	date('Y-m-d')
			),

			array(
				'id'		=>	2,
				'user_id'	=>	1,
				'content'	=>	$faker->paragraph(1),
				'created_at'=>	date('Y-m-d')
			),

			array(
				'id'		=>	3,
				'user_id'	=>	2,
				'content'	=>	$faker->paragraph(1),
				'created_at'=>	date('Y-m-d')
			)
		);

		$db->insert($data);
	}
}