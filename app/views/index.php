<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
	<title> Chat </title>
	<meta charset="utf-8">
	<link type="text/css" rel="stylesheet" href="vendor/bootstrap-dist/css/bootstrap.css">
	<style>
		.container {
			margin-top: 1%;
			width: 65%;
		}
	</style>
</head>

<body ng-controller="MainCtrl">
	<div class="container">

	<div class="form-group">
		<div class="input-group">
			<textarea rows="1" class="form-control" ng-model="data.message"></textarea>
			<span class="input-group-btn">
				<button type="button" class="btn btn-default"> Send </button>
			</span>
		</div>
	</div>

	</div>

	<script src="vendor/jquery/dist/jquery.js"></script>
	<script src="vendor/bootstrap-dist/js/bootstrap.js"></script>
	<script src="vendor/angular/angular.js"></script>
	<script src="app/app.js"></script>
</body>
</html>