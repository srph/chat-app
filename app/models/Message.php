<?php

class Message extends Eloquent {

	/**
	 * Table used by the model
	 *
	 * @var string
	 */
	protected $table = 'messages';

	/**
	 * Fields fillable by the model
	 *
	 * @var array
	 */
	protected $fillable = array('user_id', '');

	/**
	 * Model timestamps
	 *
	 * @var boolean
	 */
	public $timestamps = true;

	/**
	 * ORM with the User model
	 *
	 * @return 	User
	 */
	public function user()
	{
		return $this->belongsTo('user');
	}
}