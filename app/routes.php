<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	// return View::make('index');
	// $user = User::find(1);
	// Auth::login($user);
	return Auth::user()->messages;
});

Route::group(array('prefix' => 'api'), function()
{
	/**
	 * @link api/auth/check
	 * Returns auth status
	 */
	Route::get('auth/check', function()
	{
		return Response::json(Auth::check());
	});

	/**
	 *
	 *
	 */
	Route::get('user/{id}', function($id)
	{
		$user = User::with('messages')->find($id);
		return Response::json($user);
	});

	/**
	 *
	 *
	 */
	Route::get('auth/user', function()
	{
		// $user = Auth::user
	});

	/**
	 *
	 *
	 */
	Route::get('auth/login', function()
	{
		$username = Input::get('username');
		$password = Input::get('password');

		$data = array(
			'username'	=>	$username,
			'password'	=>	Hash::make($password)
		);

		if(Auth::attempt($data)) {
			return Response::json(array(
				'status'	=>	true
			));
		}

		return Response::json(array(
			'status'	=>	false
		));
	});

	/**
	 *
	 *
	 */
	Route::get('auth/logout', function()
	{
		Auth::logout();
	});
});